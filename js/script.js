// Відповіді на питання:

// 1.	Опишіть своїми словами що таке Document Object Model (DOM)
// Document Object Model (DOM) – це об’єктна модель документа, яка показує весь зміст сторінки у вигляді об’єктів які можна змінювати.


// 2.	Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML – дозволяє отримати HTML розмітку елементу та його дочірніх елементів, а innerText – дозволяє отримати текстову складову елементу та його дочірніх елементів


// 3.	Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Є багато способів звернутися до елемента на сторінці. На даний момент найкращими способами є три наступних:
// -	document.getElementById(id) – доступ до елемента за його id;
// -	document.querySelectorAll(css) – найуніверсальнійший метод, повертає всі елементи які задовільняють css-селектору. 
// -	document.querySelector(css) – повертає перший елемент, який задовільняє css-селектору.
// Наступні методи майже не використовуються? але ними можна користуватися і вони дуже зручні, в окремих випадках:
// -	document.retElementsByTagName(tag) – шукає елементи за даним тегом;
// -	document.retElementsByTagClassName(className) – повертає елементи які мають даний css-клас.
// -	document.retElementsByName(name) – повертає елементи із заданим атрибутом name.



// Завдання №1
// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphColor = document.querySelectorAll('p');
for (let i = 0; i < paragraphColor.length; i++) {
  paragraphColor[i].style.backgroundColor = "#ff0000";
}

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.getElementById('optionsList');
console.log(optionsList);
let parent = optionsList.parentNode;
console.log(parent);
let children = optionsList.childNodes;
console.log(children);

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

let testParagraph = document.getElementById('testParagraph');
testParagraph.textContent = 'This is a paragraph';

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

// Варіант №1 Додаємо класс тільки до безпосередньо дочірніх елементів

let parentElem = document.querySelector('.main-header');

for (let child of parentElem.children) {
  child.classList.add('nav-item');
}

// Варіант №2 Додаємо класс до всіх вкладених елементів

// let parentElem = document.querySelector('.main-header');
// let childElem = parentElem.querySelectorAll('*');

// for (let childNode of childElem) {
//   childNode.classList.add('nav-item');
// }


// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitleRemove = document.querySelectorAll('.section-title');
for (let i = 0; i < sectionTitleRemove.length; i++) {
  sectionTitleRemove[i].classList.remove('section-title');
}